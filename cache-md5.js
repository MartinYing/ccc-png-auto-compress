const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

/** 配置文件保存目录 */
const configFileDir = 'packages/ccc-png-auto-compress';
/** 配置文件名 */
const configFileName = 'ccc-png-auto-compress-caches.json';
const projectPath = Editor.Project.path || Editor.projectPath;

const Md5Mgr = {
    // 私有变量，用来缓存文件路径和 MD5 值
    _dataMap: new Map(),
    _configFilePath: '',

    // 计算文件的 MD5 值
    calculateFileMD5(filePath) {
        const hash = crypto.createHash('md5');
        const fileData = fs.readFileSync(filePath);
        hash.update(fileData);
        return hash.digest('hex');
    },

    // 保存文件路径和 MD5 到 JSON 文件中
    saveToFile2(filePath, data) {
        fs.writeFileSync(filePath, JSON.stringify(data, null, 2));
        Editor.log(`martin, compress md5 cache config saved to ${filePath}`);
    },

    // 从 JSON 文件中加载数据并缓存在 Map 中
    loadFromFile(filePath) {
        if (!fs.existsSync(filePath)) {
            return new Map();
        }
        const data = JSON.parse(fs.readFileSync(filePath, 'utf-8'));
        this._dataMap = new Map(Object.entries(data)); // 更新私有变量
    },

    // 根据文件名获取对应的 MD5
    getMD5ByFileName(filePath) {
        return this._dataMap.get(path.basename(filePath)) || null;
    },

    // 初始化方法，加载数据到 Map 中
    init() {
        this._configFilePath = path.join(projectPath, configFileDir, configFileName);
        this.loadFromFile(this._configFilePath);
    },

    // 添加文件路径和 MD5 到 Map 中
    addFile(filePath) {
        const md5 = this.calculateFileMD5(filePath);
        this._dataMap.set(path.basename(filePath), md5);
    },

    // 将 Map 中的数据保存到 JSON 文件中
    saveToFile() {
        this.saveToFile2(this._configFilePath, Object.fromEntries(this._dataMap));
    }
};
module.exports = Md5Mgr;
